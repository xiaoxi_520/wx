package com.conne.wxproject.mapper;

import com.conne.wxproject.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;


@Mapper
public interface UserInfoMapper {
    int checkUser(String openid);  //效验用户是否存在
    int insertUserInfo(UserInfo userInfo); //单个新增用户信息
    int updateUserInfo(UserInfo userInfo);//修改用户信息
    int insertUserInfoList(@Param("list") List<UserInfo> userInfo); //批量新增用户信息


}
