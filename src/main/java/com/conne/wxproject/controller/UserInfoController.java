package com.conne.wxproject.controller;


import com.conne.wxproject.common.ServerResponse;
import com.conne.wxproject.entity.UserInfo;
import com.conne.wxproject.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/user")
public class UserInfoController {
    @Autowired
    private UserInfoService userInfoService;


    /**
     * 查询用户信息是否存在
     * @param openid
     * @return
     */
    @GetMapping("/getUserInfo")
    public ServerResponse getUserInfo(String openid){
        return userInfoService.getUserInfo(openid);
    }


    /**
     * 单个新增公众号用户信息
     */
    @PostMapping("/insertUserInfo")
    public ServerResponse insertUserInfo(@RequestBody UserInfo userInfo){
        return userInfoService.insertUserInfo(userInfo);
    }

    /**
     * 修改
     */
    @PostMapping("/UpdateUserInfo")
    public ServerResponse UpdateUserInfo(@RequestBody UserInfo userInfo){
        return userInfoService.UpdateUserInfo(userInfo);
    }


    /**
     * 批量新增公众号用户信息(去除重复插入)
     */
    @PostMapping("/insertUserInfoList")
    public ServerResponse insertUserInfoList(String userList){
        return userInfoService.insertUserInfoList(userList);
    }

}
