package com.conne.wxproject.entity;

import javax.persistence.Table;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.conne.wxproject.base.BaseEntity;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

/**
 * 保存用户信息
 */

@Data
@Table(name = "wechat_userinfo")
public class UserInfo extends BaseEntity {

    @TableId(type= IdType.AUTO)
    /** 微信用户主键id */
    private Long wxUserId;

    /** 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息。 */
    private int subscribe;

    /**
     * 公众号id
     */
    private String gzid;


    /** 用户的标识，对当前公众号唯一 */
    private String openid;

    /** 微信用户昵称 */
    private String nickname;

    /** 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知 */
    private Long sex;

    /** 用户的语言，简体中文为zh_CN */
    private String language;

    /** 用户所在城市 */
    private String city;

    /** 用户所在省份 */
    private String province;

    /** 用户所在国家 */
    private String country;

    /** 用户头像 */
    private String headimgurl;

    /** 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date subscribe_time;

    /** 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。 */
    private String unionid;

    /**公众号运营者对粉丝的备注*/
    private String remark;

    /** 用户所在的分组ID（兼容旧的用户分组接口）
     */
    private Long groupid;

    /** 返回用户关注的渠道来源，ADD_SCENE_SEARCH 公众号搜索，ADD_SCENE_ACCOUNT_MIGRATION 公众号迁移，ADD_SCENE_PROFILE_CARD 名片分享，ADD_SCENE_QR_CODE 扫描二维码，ADD_SCENE_PROFILE_LINK 图文页内名称点击，ADD_SCENE_PROFILE_ITEM 图文页右上角菜单，ADD_SCENE_PAID 支付后关注，ADD_SCENE_WECHAT_ADVERTISEMENT 微信广告，ADD_SCENE_OTHERS 其他 */
    private String subscribe_scene;


    /** 二维码扫码场景（开发者自定义）
     */
    private String qr_scene;


    /** 二维码扫码场景描述（开发者自定义）
     */
    private String qr_scene_str;

    /** 公众号名称 */
    private String publicAccount;

    /** 最后互动时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastInteractionTime;


    /** 0表示未删除1表示删除 */
    private Long isDel;





}
