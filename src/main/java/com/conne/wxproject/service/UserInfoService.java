package com.conne.wxproject.service;


import com.conne.wxproject.common.ServerResponse;
import com.conne.wxproject.entity.UserInfo;




public interface UserInfoService{
    ServerResponse getUserInfo(String openid);
    ServerResponse insertUserInfo(UserInfo userInfo);
    ServerResponse UpdateUserInfo(UserInfo userInfo);
    ServerResponse insertUserInfoList(String userList);
}
