package com.conne.wxproject.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.conne.wxproject.common.ResponseCode;
import com.conne.wxproject.common.ServerResponse;
import com.conne.wxproject.common.SystemEnums;
import com.conne.wxproject.entity.UserInfo;
import com.conne.wxproject.mapper.UserInfoMapper;
import com.conne.wxproject.service.UserInfoService;
import com.conne.wxproject.utils.TimeStampDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("all")
@Service
public class UserInfoImpl implements UserInfoService {

    @Autowired
    private UserInfoMapper userInfoMapper;


    /**
     * 查询公众号信息是否存在
     * @param openid
     * @return
     */
    @Override
    public ServerResponse getUserInfo(String openid) {
        if(ObjectUtils.isEmpty(openid)){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        int count = userInfoMapper.checkUser(openid);
        if(count>SystemEnums.A.getNum()){
            return ServerResponse.createBySuccessMessage("此公众号用户信息已存在！");
        }
        return ServerResponse.createBySuccessMessage("此公众号用户信息不存在！");
    }


    /**
     * 单个新增公众号用户信息
     * @param userInfo
     * @return
     */
    @Override
    public ServerResponse insertUserInfo(UserInfo userInfo) {
        Date date1 = null;
        int count = userInfoMapper.checkUser(userInfo.getOpenid());
        if(StringUtils.isNotEmpty(userInfo.getUnionid())&&count==SystemEnums.A.getNum()){
            //用户不存在则新增
            /**
             * 时间的处理
             */
            String format = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            Long lg = userInfo.getSubscribe_time().getTime();
            String date = Long.toString(lg);
            String dateString = TimeStampDate.timeStampToDate(date);
            try {
                date1 = sdf.parse(dateString);
                System.out.println("时间:"+date1);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            //处理微信头像乱码
            String headimgurl = userInfo.getHeadimgurl();
            String regExp= "[`~!@#$%^&*()+=|{}';',\\\\\\[\\]<>?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";
            Pattern p = Pattern.compile(regExp);
            Matcher m = p.matcher(headimgurl);
            headimgurl = m.replaceAll("").trim();
            userInfo.setHeadimgurl(headimgurl);
            userInfo.setSubscribe_time(date1);
            userInfo.setCreateTime(new Date());
            userInfo.setUpdateTime(new Date());
            int users = userInfoMapper.insertUserInfo(userInfo);
            if(users>SystemEnums.A.getNum()){
                return ServerResponse.createBySuccessMessage("单个新增公众号用户信息成功！");
            }
            return ServerResponse.createByErrorMessage("新增失败！");
        }else if(count>SystemEnums.A.getNum()){
            /**
             * 时间的处理
             */
            String format = "yyyy-MM-dd HH:mm:ss";
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            if(userInfo.getSubscribe_time()==null){
                userInfo.setUpdateTime(new Date());
                int num =  userInfoMapper.updateUserInfo(userInfo);
                if(num>SystemEnums.A.getNum()){
                    return ServerResponse.createBySuccessMessage("修改成功！");
                }
                return ServerResponse.createByErrorMessage("用户信息不存在！");
            }else {
                String lg = String.valueOf(userInfo.getSubscribe_time().getTime());
                String dateString = TimeStampDate.timeStampToDate(lg);
                Date date = null;
                try {
                    date = sdf.parse(dateString);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                userInfo.setSubscribe_time(date);
                userInfo.setUpdateTime(new Date());
                //处理微信头像乱码
                String headimgurl = userInfo.getHeadimgurl();
                String regExp= "[`~!@#$%^&*()+=|{}';',\\\\\\[\\]<>?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";
                Pattern p = Pattern.compile(regExp);
                Matcher m = p.matcher(headimgurl);
                headimgurl = m.replaceAll("").trim();
                userInfo.setHeadimgurl(headimgurl);
                int num =  userInfoMapper.updateUserInfo(userInfo);
                if(num>SystemEnums.A.getNum()){
                    return ServerResponse.createBySuccessMessage("修改成功！");
                }
                return ServerResponse.createByErrorMessage("用户信息不存在！");
            }
        }
        return ServerResponse.createByErrorMessage("参数错误:缺少关键字段！");


    }


    /**
     * 修改公众号用户信息
     * @param userInfo
     * @return
     */
    @Override
    public ServerResponse UpdateUserInfo(UserInfo userInfo) {
        if(ObjectUtils.isEmpty(userInfo)){
            return ServerResponse.createByErrorCodeMessage(ResponseCode.ILLEGAL_ARGUMENT.getCode(),ResponseCode.ILLEGAL_ARGUMENT.getDesc());
        }
        int count = userInfoMapper.checkUser(userInfo.getOpenid());
        if(count>SystemEnums.A.getNum()){
            //修改
            userInfo.setUpdateTime(new Date());
            int num =  userInfoMapper.updateUserInfo(userInfo);
            if(num>SystemEnums.A.getNum()){
                return ServerResponse.createBySuccessMessage("修改成功！");
            }
            return ServerResponse.createByError();
        }
        return ServerResponse.createByErrorCodeMessage(ResponseCode.USER_NOT_EXISTS.getCode(),ResponseCode.USER_NOT_EXISTS.getDesc());
    }


    /**
     * 批量新增公众号用户信息
     * @param userList
     * @return
     */
    @Override
    public ServerResponse insertUserInfoList(String userList) {
        List<UserInfo> userInfos = JSONObject.parseArray(userList, UserInfo.class);
        ArrayList<UserInfo> userInfoArrayList = new ArrayList<>();
        AtomicInteger flag = new AtomicInteger();
        AtomicInteger atomicInteger = new AtomicInteger();
        userInfos.forEach(userInfoEntity -> {
            if (!SystemEnums.B.getNum().equals(getInsertNum(userInfoEntity))){
                flag.getAndIncrement();
            }else {
                int count = userInfoMapper.checkUser(userInfoEntity.getOpenid());
                if(count==SystemEnums.A.getNum()){
                    userInfoEntity.setCreateTime(new Date());
                    atomicInteger.getAndIncrement();
                    userInfoArrayList.add(userInfoEntity);
                }

            }
        });
        if (userInfoArrayList.size()> SystemEnums.A.getNum()
                && userInfoMapper.insertUserInfoList(userInfoArrayList) > SystemEnums.A.getNum()){
            return ServerResponse.createBySuccessMessage("批量新增公众号用户信息成功！");
        }
        return ServerResponse.createByErrorCodeMessage(ResponseCode.USER_EXISTS.getCode(),ResponseCode.USER_EXISTS.getDesc());

    }

    /**
     *修改条件
     * @param userInfo
     * @return
     */
    @SuppressWarnings("all")
    private Integer getInsertNum(UserInfo userInfo){
        if(ObjectUtils.isEmpty(userInfo.getSubscribe())){
            return SystemEnums.C.getNum(); //订阅该公众号标识不能为空
        }
        if(ObjectUtils.isEmpty(userInfo.getUnionid())){
            return SystemEnums.D.getNum(); //OpenId不能为空
        }
        if(ObjectUtils.isEmpty(userInfo.getNickname())){
            return SystemEnums.E.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getSex())){
            return SystemEnums.F.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getCountry())){
            return SystemEnums.G.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getLanguage())){
            return SystemEnums.H.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getHeadimgurl())){
            return SystemEnums.I.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getSubscribe_time())){
            return SystemEnums.J.getNum();
        }
        if(ObjectUtils.isEmpty(userInfo.getUnionid())){
            return SystemEnums.K.getNum();
        }
        return SystemEnums.B.getNum();
    }





}
