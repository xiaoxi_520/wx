package com.conne.wxproject.utils;


import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStampDate {

    /**
     * 10位时间戳转换成日期格式字符串
     * 精确到秒的字符串
     */
    public static String timeStampToDate(String seconds) {
        String format = "yyyy-MM-dd HH:mm:ss";
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return "";
        }
        if(format == null || format.isEmpty()){
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String date = sdf.format(new Date(Long.valueOf(seconds+"000")));
        return date;
    }

}
