package com.conne.wxproject.common;

@SuppressWarnings("all")
public enum  SystemEnums {
    K(10), L(11), M(12), N(13), O(14), P(15), Q(16), R(17),S(18),
    A(0), B(1), C(2), D(3), E(4), F(5), G(6), H(7), I(8), J(9),
    T(19),U(20),V(21),W(22),X(23),Y(24),Z(25)
    ;
    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    private Integer num;


    SystemEnums(Integer num) {
        this.num = num;

    }

    public static SystemEnums getState(Integer i) {
        for (SystemEnums opCode : SystemEnums.values()) {
            if (opCode.getNum()==i) {
                return opCode;
            }
        }
        return null;
    }

}
