package com.conne.wxproject.common;


@SuppressWarnings("all")
public enum  ResponseCode {
    SUCCESS(200, "SUCCESS"),//成功
    ERROR(500, "ERROR"),//失败
    INVALID_OPEN_ID(40013,"INVALID_OPEN_ID"),//不合法的open_Id
    JSON_ERROR(40014,"JSON_ERROR"),//解析JSON内容错误
    ILLEGAL_ARGUMENT(40015, "ILLEGAL_ARGUMENT"),//参数错误
    USER_EXISTS(40016,"USER_EXISTS"),//用户已经存在
    USER_NOT_EXISTS(40017,"USER_NOT_EXISTS"); //用户不存在
    private final int code;
    private final String desc;


    ResponseCode(int code, String desc) {
        this.code = code;

        this.desc = desc;
    }

    public int getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}