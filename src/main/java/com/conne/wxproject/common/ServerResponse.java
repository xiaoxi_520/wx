package com.conne.wxproject.common;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.annotate.JsonIgnore;
import java.io.Serializable;

/** 通用数据端的响应对象
 */
@SuppressWarnings("all")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class ServerResponse <T> implements Serializable {
    private int status;
    private String msg;
    private T data;//t:可以指定泛型里面的内容，也可以不指定泛型里面的强制内容

    //私有构造器是为了在封装public方法调用时比较简明，通用
    private ServerResponse(int status) {
        this.status = status;
    }

    private ServerResponse(int status, T data) {
        this.status = status;
        this.data = data;
    }

    private ServerResponse(int status, String msg, T data) {
        this.status = status;
        this.msg = msg;
        this.data = data;
    }

    private ServerResponse(int status, String msg) {
        this.status = status;
        this.msg = msg;
    }

    //如果status=ResponseCode.SUCCESS.getCode=0，返回为true
    // @JsonIgnore:isSuccess字段在序列化后不会显示在josn里面，而低下public get开头的都会显示在json里面
    @JsonIgnore
    public boolean isSuccess() {
        return this.status == ResponseCode.SUCCESS.getCode();
    }


    //对外开放的方法
    public int getStatus(){
        return  status;
    }

    public T getData(){
        return data;
    }
    public String getMsg(){
        return  msg;
    }


    /**
     * 创建成功的响应
     * @param <T>
     * @return
     */
    //创建一个对象通过成功的,code=0响应是成功的，返回一个SUCCESS
    public static <T> ServerResponse<T> createBySuccess(){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode());
    }

    //创建一个对象,成功的话返回一个文本供前端使用
    public static <T> ServerResponse<T> createBySuccessMessage(String msg){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg);
    }

    //创建一个成功的服务器响应,把data填充进去
    public static <T> ServerResponse<T> createBySuccess(T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),data);
    }

    //上面2、3构造器传string作为一个data，会调用这个方法
    public static <T> ServerResponse<T> createBySuccess(String msg,T data){
        return new ServerResponse<T>(ResponseCode.SUCCESS.getCode(),msg,data);
    }


    /**
     * 创建失败的响应
     */
    public static <T> ServerResponse<T> createByError(){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),ResponseCode.ERROR.getDesc());
    }

    //返回提示的错误
    public static <T> ServerResponse<T> createByErrorMessage(String errorMessage){
        return new ServerResponse<T>(ResponseCode.ERROR.getCode(),errorMessage);
    }
    //把code做成变量的服务端响应
    public static <T> ServerResponse<T> createByErrorCodeMessage(int errorCode,String errorMessage){
        return new ServerResponse<T>(errorCode,errorMessage);

    }
}
