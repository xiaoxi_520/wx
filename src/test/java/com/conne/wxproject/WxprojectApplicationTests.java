package com.conne.wxproject;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;

@SpringBootTest
class WxprojectApplicationTests {

    @Test
    String contextLoads() {
        return null;
    }


    public static void main(String[] args) {
        //1. 可以在中括号内加上任何想要删除的字符，实际上是一个正则表达式
        String regExp= "[`~!@#$%^&*()+=|{}';',\\\\\\[\\]<>?~！@#￥%……&*（）——+|{}【】‘；：”“’。， 、？]";
        //2. 这里是将特殊字符换为空字符串,""代表直接去掉
        String replace = "";
        //3. 要处理的字符串
        String str = "//中国.lov:e,you\\\\";
        System.out.println("原数据为:"+str);
        str = str.replaceAll(regExp,replace);
        System.out.println("去除特殊字符后的值："+ str);
    }



    /**
     * 10位时间戳转换成日期格式字符串
     * 精确到秒的字符串
     */
    public static String timeStamp2Date(String seconds) {
        String format = "yyyy-MM-dd HH:mm:ss";
        if(seconds == null || seconds.isEmpty() || seconds.equals("null")){
            return "";
        }
        if(format == null || format.isEmpty()){
            format = "yyyy-MM-dd HH:mm:ss";
        }
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String date = sdf.format(new Date(Long.valueOf(seconds+"000")));
        System.out.println(date);
        return date;
    }




}
